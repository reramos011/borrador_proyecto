// Se importa fs para trabajar con ficheros
const fs = require('fs');

function writeUserDataToFile(users) {
  var jsonUserData = JSON.stringify(users);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(error) {
      if (error) {
        console.log(error)
        res.sendStatus(500);
      } else {
        console.log("Usuario persistido")
        res.sendStatus(201);
      }
    }
  );
}

// Exponemos el método para poderlo usar desde otros ficheros
module.exports.writeUserDataToFile = writeUserDataToFile;
