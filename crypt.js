// Se importa la librería bcrypt
const bcrypt = require('bcrypt');

function hash(data) {
  console.log("Hashing data");
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFormUserInPlainText, passwordFromDBHash) {
  console.log("Checking password: " + passwordFormUserInPlainText + " - " + passwordFromDBHash);

  return bcrypt.compareSync(passwordFormUserInPlainText, passwordFromDBHash);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
