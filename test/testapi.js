// Librería base de test unitarios
const mocha = require('mocha');
// Librería para realizar aserciones
const chai = require('chai');
// Librería para usar un cliente http en las aserciones de chai
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

describe('First test',
  function() {
    it('Test that DuckDuckgo works', function (done) {
        chai.request('http://www.duckdukgo.com')
          .get('/')
          .end(
            function(err, res) {
              // console.log("Request has finished");
              // console.log(err);
              // Se realiza una aserción para verificar que la petición devuelve status 200
              res.should.have.status(200);
              done();
            }
          );
      }
    );
  }
);

describe('Test de API Usuarios',
  function() {
    it('Prueba que la API de Usuarios responde correctamente', function (done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function(err, res) {
              // console.log("Request has finished");
              // console.log(err);
              // Se realiza una aserción para verificar que la petición devuelve status 200
              res.should.have.status(200);
              // Se realiza una aserción para verificar que devuelve una propiedad msg con el valor esperado
              res.body.msg.should.be.eql("Hola desde apitechu");
              done();
            }
          );
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos', function (done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err, res) {
              // console.log("Request has finished");
              // console.log(err);
              // Se realiza una aserción para verificar que la petición devuelve status 200
              res.should.have.status(200);
              // Comprobamos que lo que devuelve es de tipo array
              res.body.users.should.be.a('array');
              // Comprobamos que cada elemento tiene una propiedad email y otra first_name
              for (user of res.body.users) {
                user.should.have.property('email');
                user.should.have.property('first_name');
              }
              done();
            }
          );
      }
    );
  }
);
