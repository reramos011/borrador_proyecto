// Se importa la librería
const express = require('express');
// Se arranca el framework express
const app = express();
// se importa el controlador de usuarios
const userController = require("./controllers/UserController");
// Se importa el controlador de autenticacion
const authController = require("./controllers/AuthController");

// Se configura express para que procese siempre el body como json
app.use(express.json());

// Se define un puerto
const port = process.env.PORT || 3000;
// Se levanta la app en el puerto definido
app.listen(port);
console.log("API escuchando en el puerto BIP BIP " + port);

// Implementación de un metodo hola mundo
app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hola desde apitechu"});
  }
);

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function (req, res) {
      console.log("Parámetros", req.params);
      console.log("Query String", req.query);
      console.log("Headers", req.headers);
      console.log("Body", req.body);
  }
);

// Funciones crud usuarios
app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.post('/apitechu/v1/users', userController.postUserV1);
app.post('/apitechu/v2/users', userController.postUserV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

//Funciones autenticacion
app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v1/logout/:id', authController.logoutV1);
app.post('/apitechu/v2/login', authController.loginV2);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);
