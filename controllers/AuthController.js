//Se importa el fichero io que contiene funciones de entrada/salida creadas por nosotros
const io = require('../io');
//Se importa el fichero io que contiene funciones de entrada/salida creadas por nosotros
const crypt = require('../crypt');
// Se importa el cliente http
const requestJson = require('request-json');
// Url base Mlab
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechurrs9ed/collections/";
// MLab apiKey
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req, res) {
  console.log(" POST /apitechu/v1/login con los valores " + req.body.email + "," + req.body.password);

  // Se recuperan los usuarios
  var users = require('../usuarios.json');
  // Se valida si existe un usuario con el email y password recibidos. Devolverá -1 si no se encuentra
  var index = users.findIndex(user => (user.email == req.body.email) && (user.password == req.body.password));
  var result;
  if (index == -1) {
    result = {"mensaje" : "Login incorrecto"};
  } else {
    users[index].logged = true;
    io.writeUserDataToFile(users);
    result = {"mensaje" : "Login correcto", "idUsuario" : users[index].id}
  }

  res.send(result);
}

function logoutV1(req, res) {
  console.log("POST /apitechu/v1/logout para el id " + req.params.id);

  // Se recuperan los usuarios
  var users = require('../usuarios.json');
  // Se valida si existe el usuario para hacer el logout. Si no existe devolverá -1
  var index = users.findIndex(user => (user.id == req.params.id));
  var result;
  if (index == -1) {
    result = {"mensaje" : "Logout incorrecto"};
  } else {
    delete users[index].logged;
    io.writeUserDataToFile(users);
    result = {"mensaje" : "Logout correcto"};
  }

  res.send(result);
}

function loginV2(req, res) {
  console.log(" POST /apitechu/v2/login con los valores " + req.body.email + "," + req.body.password);

  // Se recupera el usuario filtrando por el email
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"email" : "' + req.body.email + '"}';
  var url = "user?" + query + "&" + mLabAPIKey;
  httpClient.get(url,
    function (err, resMLab, body) {
      if (err) {
        var response = {
          "mensaje" : "No es posible acceder en este momento. Intentelo de nuevo pasado un tiempo."
        }
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          var user = body[0];
          // Se comprueba el password
          if(crypt.checkPassword(req.body.password, user.password)) {
            // Si el password es correcto, se marca el usuario como autenticado
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put(url, JSON.parse(putBody),
              function (err, resMLab, body) {
                console.log(body);
                if (err) {
                  var response = {
                    "mensaje" : "No es posible acceder en este momento. Intentelo de nuevo pasado un tiempo."
                  }
                  res.status(500);
                } else {
                  var response = {"mensaje" : "Login correcto", "idUsuario" : user.id};
                  res.status(200);
                }
                res.send(response);
              }
            );
          } else {
            var response = {"mensaje" : "Login incorrecto"};
            res.status(401);
            res.send(response);
          }
        } else {
          var response = {"mensaje" : "Login incorrecto"};
          res.status(401);
          res.send(response);
        }
      }
    }
  );
}

function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout para el id " + req.params.id);

  // Se actualiza en bbdd el usuario por su id
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"id" : ' + req.params.id + '}';
  var url = "user?" + query + "&" + mLabAPIKey;
  var putBody = '{"$unset":{"logged":""}}';
  httpClient.put(url, JSON.parse(putBody),
    function (err, resMLab, body) {
      console.log(body);
      if (err) {
        var response = {
          "mensaje" : "No es posible acceder en este momento. Intentelo de nuevo pasado un tiempo."
        }
        res.status(500);
      } else {
        if(body.n == 1) {
          var response = {"mensaje" : "Logout correcto"};
          res.status(200);
        } else {
          var response = {
            "mensaje" : "Logout incorrecto."
          }
          res.status(500);
        }
      }
      res.send(response);
    }
  );
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
