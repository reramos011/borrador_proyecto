// Se carga el fichero de configuracion
require('dotenv').config();
// Se importa la libreria isnumeric
const isNumeric = require("isnumeric");
//Se importa el fichero io que contiene funciones de entrada/salida creadas por nosotros
const crypt = require('../crypt');
//Se importa el fichero crypt que contiene funciones de encriptación
const io = require('../io');
// Se importa el cliente http
const requestJson = require('request-json');
// Url base Mlab
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechurrs9ed/collections/";
// MLab apiKey
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

//  res.sendFile('usuarios.json', {root: __dirname});
  var users = require('../usuarios.json');

  // Recogen las variables de la requests
  var top = req.query.$top;
  var count = req.query.$count;

  var data = {};
  // Si viene count a true, se devuelve el solo el valor del count
  if (count == "true") {
    var countData = users.length;
    data.count = countData;
  }

  // Si viene el top, se valida
  if (top) {
    if (!isNumeric(top)) {
      // Que el top tenga un valor numerico
      data.errorMsg = "El valor del parametro $top debe ser numérico";
    } else if(top <= 0 || top > users.length) {
      // Que el top no sobrepase los límites del array
      data.errorMsg = "El valor del parametro $top debe ser mayor que cero y menor que el total (usar el parametro count=true para conocer el total)";
    } else {
        data.users = users.slice(0, top);
    }
  } else {
        data.users = users;
  }
  res.send(data);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("user?" + mLabAPIKey,
    function (err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/" + req.params.id);
  var httpClient = requestJson.createClient(baseMlabURL);
  var query = 'q={"id" : ' + req.params.id + '}';
  var url = "user?" + query + "&" + mLabAPIKey;
  console.log(url);
  httpClient.get(url,
    function (err, resMLab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function postUserV1(req, res) {
  console.log("POST /apitechu/v1/users");
  console.log(req.headers.first_name);
  console.log(req.headers.last_name);
  console.log(req.headers.email);

  // Se recogen los datos de las cabeceras
  var newUser = {
    "first_name": req.headers.first_name,
    "last_name" : req.headers.last_name,
    "email" : req.headers.email
  };

  var users = require('../usuarios.json');
  users.push(newUser);
  // Se recogen los datos del body
  users.push(req.body);
  console.log("Usuario añadido");
  io.writeUserDataToFile(users);
  res.sendStatus(201);
}

function postUserV2(req, res) {
  console.log("POST /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMlabURL);
  var url = "user?" + mLabAPIKey;
  console.log(url);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  };

  httpClient.post(url, newUser,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error creando usuario"
        }
        res.status(500);
      } else {
        var response = {
          "msg" : "Usuario creado correctamente"
        }
        res.status(201);
      }
      res.send(response);
    }
  );
}

// Metodo de borrado 6 -> El elemento a borrar se localiza usando findIndex
function deleteUserV1(req, res) {
    console.log("DELETE  /apitechu/v1/users/:id")
    console.log("La id del elemento a borrar es " + req.params.id);

    var users = require('../usuarios.json');
    const index = users.findIndex(user => user.id == req.params.id);
    console.log("Encontrado elemento en la posicion " + index);
    users.splice(index, 1);

    io.writeUserDataToFile(users);
    console.log("usuario borrado");
    res.send({"msg": "Usuario borrado"});
}

// Metodo de borrado 1 -> El elemento a borrar se localiza utilizando el id como index del array.
function deleteUserByIndex(req, res) {
  console.log("DELETE  /apitechu/v1/users/:id")
  console.log("La id del elemento a borrar es " + req.params.id);

  var users = require('../usuarios.json');
  users.splice(req.params.id - 1, 1);
  console.log("usuario borrado");
  io.writeUserDataToFile(users);
}

// Metodo de borrado 2 -> El elemento a borrar se localiza iterando con un for clásico
function deleteUserByClassicFor (req, res) {
  console.log("DELETE  /apitechu/v1/users/:id")
  console.log("La id del elemento a borrar es " + req.params.id);

  var users = require('../usuarios.json');
  for (var i = 0; i < users.length; i++) {
    console.log("Comprobando elemento " + i);
    if (req.params.id == users[i].id) {
      console.log("Elemento con id " + users[i].id + " encontrado")
      users.splice(i, 1);
      break;
    }
  }

  io.writeUserDataToFile(users);
  console.log("usuario borrado");
}

// Metodo de borrado 3 -> El elemento a borrar se localiza iterando con un for each
function deleteUserByForEach (req, res) {
		console.log("DELETE  /apitechu/v1/users/:id")
    console.log("La id del elemento a borrar es " + req.params.id);

    var users = require('../usuarios.json');
    users.forEach(function(user, i) {
      console.log("Comprobando elemento " + user.id);
      if (req.params.id == user.id) {
        console.log("Elemento con id " + user.id + " encontrado ");
        users.splice(i, 1);
        // No se puede hacer break. Hay que lanzar una excepción para salir
      }
    });
    io.writeUserDataToFile(users);
    console.log("usuario borrado");
}

// Metodo de borrado 4 -> El elemento a borrar se localiza iterando con un for in
function deleteUserByForIn(req, res) {
  // No se debe utilizar para recorrer arrays. Se utiliza para iterar propiedades de un objeto.

  console.log("DELETE  /apitechu/v1/users/:id")
  console.log("La id del elemento a borrar es " + req.params.id);

  var users = require('../usuarios.json');
  for (index in users) {
    console.log("Comprobando elemento " + index);
    if (req.params.id == users[index].id) {
      console.log("Elemento con id " + users[index].id + " encontrado")
      users.splice(index, 1);
      break;
    }
  }

  io.writeUserDataToFile(users);
  console.log("usuario borrado");
}


// Metodo de borrado 5 -> El elemento a borrar se localiza iterando con un for of
function deleteUSerByForOf (req, res) {
    console.log("DELETE  /apitechu/v1/users/:id")
    console.log("La id del elemento a borrar es " + req.params.id);

    var users = require('../usuarios.json');
    // Con array.entries conseguimos un objeto iterable compuesto por el index y el elemento
    for (entry of users.entries())  {
      var index = entry[0];
      var id = entry[1].id;
      console.log("Comprobando elemento " + id);
      if (req.params.id == id) {
        console.log("Elemento con id " + id + " encontrado");
        users.splice(index, 1);
        break;
      }
    }
    io.writeUserDataToFile(users);
    console.log("usuario borrado");
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2
module.exports.postUserV1 = postUserV1;
module.exports.postUserV2 = postUserV2;
module.exports.deleteUserV1 = deleteUserV1;
